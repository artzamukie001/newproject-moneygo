import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'

import Login from './src/Page/Login'
import Register from './src/Page/Register'

class Router extends Component {
    render() {
        return (
                <NativeRouter>
                    <Switch>
                        <Route exact path="/Login" component={Login} />
                        <Route exact path="/Register" component={Register} />
                        
                        <Redirect to="/Login" />
                    </Switch>
                </NativeRouter>
        )
    }
}

export default Router
