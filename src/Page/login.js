import React from 'react'
import { View, Text, TochableOpacity } from 'react-native'
import { Icon, IputItem, WhiteSpace } from '@ant-design/react-native'
import { Containers, TextMoneyGo, Logo, ItemInput, Imagebackground, AreaBox, Footers, ButonLogin, TextGeneral, TextInput } from '../Components/StyledComponents'
import Background from '../image/gold3.jpg'
import BackgroundLogo from '../image/logo.jpg'

class Login extends React.Component {

    goToRegister = () => {
        this.props.history.push('./Register')
    }

    render() {
        return (
            <Containers>
                <Imagebackground source={Background}>
                    <AreaBox>
                        <TextMoneyGo>Money Go</TextMoneyGo>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <Logo source={BackgroundLogo}></Logo>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <TextInput>Username</TextInput>
                        <ItemInput></ItemInput>
                        <WhiteSpace></WhiteSpace>
                        <TextInput>Password</TextInput>
                        <ItemInput></ItemInput>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <ButonLogin onPress={this.goToRegister}>Login</ButonLogin>
                    </AreaBox>
                </Imagebackground>
               
                    <Footers>
                    <TextGeneral>Rejister</TextGeneral>
                    </Footers>
                
            </Containers>  
        )
    }
}

export default Login
