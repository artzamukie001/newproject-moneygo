import React from 'react'
import { View, Text, TochableOpacity } from 'react-native'
import { Icon, IputItem, WhiteSpace } from '@ant-design/react-native'
import { Containers, TextMoneyGo, Logo, ItemInput, Imagebackground, AreaBox, Footers, ButonLogin,TextGeneral,TextInput } from '../Components/StyledComponents'
import Background from '../image/gold3.jpg'

class Register extends React.Component {
    render() {
        return (
            <Containers>
                <Imagebackground source={Background}>
                    <AreaBox>
                        <TextMoneyGo>Register</TextMoneyGo>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <TextInput>Email</TextInput>
                        <ItemInput></ItemInput>
                        <WhiteSpace></WhiteSpace>
                        <TextInput>Password</TextInput>
                        <ItemInput></ItemInput>
                        <WhiteSpace></WhiteSpace>
                        <TextInput>Firstname</TextInput>
                        <ItemInput></ItemInput>
                        <WhiteSpace></WhiteSpace>
                        <TextInput>Lastname</TextInput>
                        <ItemInput></ItemInput>

                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <ButonLogin>Save</ButonLogin>
                    </AreaBox>
                </Imagebackground>
            </Containers>
        )
    }
}

export default Register