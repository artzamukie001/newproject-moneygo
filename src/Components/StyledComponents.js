import styledComponents from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native'


export const Containers = styledComponents.View` 
flex : 1;
`
export const Imagebackground = styledComponents.ImageBackground`
align-items:center;
width:null;
height:null;
flex:1;
padding-top:35px;
`
export const AreaBox = styledComponents.View`
background-color: rgba(0,0,0,0.3);
align-items:center;
width:300;
height:505
`
export const Footers = styledComponents.View`
background-color: rgba(0,0,0,0);
align-items:center;
`

export const TextMoneyGo = styledComponents.Text `
font-size: 50px;
font-weight: bold;
color: white;
`
export const TextGeneral = styledComponents.Text`
font-size: 30px;
font-weight: bold;
`
export const TextInput = styledComponents.Text`
font-size: 20px;
font-weight: bold;
color: white;
`

export const Logo = styledComponents.ImageBackground`
background-color: white;
width: 150;
height: 150;
border-radius: 300;
`

export const ItemInput = styledComponents(InputItem)`
background-color: white;
    color: black; 
    padding-left: 20px;
    height: 100%;
`

export const ButonLogin = styledComponents(Button)`

`


